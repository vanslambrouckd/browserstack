#!/usr/bin/python

import os
import yaml
import sys
import base64
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.remote.webdriver import WebDriver

def read_browsers(filename):
    with open(filename, 'r') as stream:
        try:
            tmp = yaml.load(stream)
            #print(browsers)
            return tmp['browsers']            
        except yaml.YAMLError as exc:
            print(exc)

def read_urls(filename):
    urls = []
    with open(filename, 'r') as stream:
        try:
            tmp = yaml.load(stream)
            for url in tmp['urls']:
                tmp = url.split(' ', 1)
                url = {}
                url['name'] = tmp[0]
                url['url'] = tmp[1]
                urls.append(url)
            return urls
        except yaml.YAMLError as exc:
            print(exc)

def read_resolutions(filename):
    with open(filename, 'r') as stream:
        try:
            tmp = yaml.load(stream)
            #print(browsers)
            return tmp['resolutions']            
        except yaml.YAMLError as exc:
            print(exc)

def read_credentials(filename):
    file = open(filename, 'r')
    str = file.read();
    tmp = str.split(':')
    ret = {};
    ret['USERNAME'] = tmp[0]
    ret['BROWSERSTACK_ACCESS_KEY'] = tmp[1]
    return ret

def take_screenshot(webdriver, file_name = "sample.png"):
    """
    @param webdriver: WebDriver handler.
    @type webdriver: WebDriver
    @param file_name: Name to label this screenshot.
    @type file_name: str 
    """
    if isinstance(webdriver, WebDriver):
        base64_data = webdriver.get_screenshot_as_base64()
        screenshot_data = base64.decodestring(base64_data)
        screenshot_file = open(file_name, "w")
        screenshot_file.write(screenshot_data)
        screenshot_file.close()
    else:
        webdriver.save_screenshot(filename)

credentials = read_credentials(os.path.expanduser("~")+'/.browserstack')
# try:
#    URL = sys.argv[1]
#URL = "http://3976b934.ngrok.io/betapor"
# except IndexError:
#     print("Pleaes a url.")
#     sys.exit(1)

def get_resolution(str):
    tmp = str.split("x")
    dimensions = {}
    dimensions['x'] = tmp[0]
    dimensions['y'] = tmp[1]
    return dimensions

browsers = read_browsers("indiegroup_browsers.yaml")
urls = read_urls("indiegroup_browsers.yaml")
resolutions = read_resolutions("indiegroup_browsers.yaml")
# for resolution in resolutions:
#     dimensions = get_resolution(resolution)
#     print(dimensions)

for url in urls:   
    print "--------------------------------------"
    print "url:"+url['url']
    print "--------------------------------------"
    for browser in browsers:
        for resolution in resolutions:            
            dimensions = get_resolution(resolution)
            filename = './screenshots/'
            filename += '['+url['name']+']'
            filename += '-'
            filename += '['+browser["browser"]+"-"+browser["browser_version"]+']'+"-["+browser["os"]+"-"+browser["os_version"]+']'
            filename += '-'
            filename += '['+dimensions['x']+"x"+dimensions['y']+']'            
            filename += ".png"
            if (os.path.isfile(filename)):
                print "file exists, skipped: "+filename
            else:
                print "retrieving "+filename
                driver = webdriver.Remote(
                command_executor = 'http://'+credentials['USERNAME']+':'+credentials['BROWSERSTACK_ACCESS_KEY']+'@hub.browserstack.com/wd/hub',
                desired_capabilities = browser)
                driver.get(url['url'])
                #driver.manage().window().resize(dimensions[x], dimensions[y])                
                if (browser['browser'] != 'Safari'):
                    driver.set_window_size(dimensions['x'], dimensions['y'])
                #driver.manage().window().setSize(1024,768)
                take_screenshot(driver, filename)    